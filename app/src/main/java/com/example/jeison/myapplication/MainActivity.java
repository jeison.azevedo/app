package com.example.jeison.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.math.RoundingMode;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void sorteio(View view){

        TextView texto = findViewById(R.id.caixa);
        Random gerador = new Random();
        String b = texto.getText().toString();
        int a = gerador.nextInt(10);
        texto.setText(b+a);

    }
}
